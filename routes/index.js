var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'Express' });
});

// get receipts
const r = require('rethinkdb'); 
const db_host = 'db.4ekist.ru';
const db_port = '28015';

var connection = null;

router.get('/receipts', function(req, res) {
  key = req.query.key;
  if (key != null) {
    console.log(key);
    //запрос к БД для получения чека с позициями по ключу key
    r.connect({host: db_host, port: db_port},function(err, conn) {
      if (err) throw err; 
      connection = conn;
      r.db('receipts') 
      .table('testuserid_fts_infos')
      .filter({key: key})
      .orderBy(r.desc('dateTime')) 
      .limit(10) 
      .innerJoin( 
      r.db('receipts').table('testuserid_fts_items').orderBy('pos'), 
      function (infos, items) { 
      return infos('key').eq(items('key')); 
      }).without({left: 'key'}) 
      .zip().run(connection, function(err, cursor) {
      if (err) throw err;
        cursor.toArray(function(err, result) {
            if (err) throw err;
            receipts = result;
            console.log(receipts);
        });
      });
    }); 
  }
  else {
    console.log('123');
    r.connect({host: db_host, port: db_port},function(err, conn) {
      if (err) throw err; 
      connection = conn;
      r.db('receipts') 
        .table('testuserid_fts_infos') 
        .orderBy(r.desc('dateTime')) 
        .limit(10) 
        .innerJoin( 
      r.db('receipts').table('testuserid_fts_items').orderBy('pos'), 
      function (infos, items) { 
      return infos('key').eq(items('key')); 
      }).without({left: 'key'}) 
      .zip().run(connection, function(err, cursor) {
        // ответ от БД  
        if (err) throw err;
          cursor.toArray(function(err, result) {
              if (err) throw err;
              receipts = result;
              console.log(receipts);
        });
      });
    });      
  };

  res.render(
    'receipts', {
    page:'Home', menuId:'home', receipts: receipts
  }); 
});
module.exports = router;